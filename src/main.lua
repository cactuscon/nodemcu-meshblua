_G.log = require('log')
_G.bind = require('bind')
-- _G.file = require('file-safe')
_G.Timer = require('timer-auto')
-- _G.print = function()end

_G.mqttClient = require('mqtt-client'):new({
  host="192.168.1.2",
})

-- require('protect-global')

Timer.softwd(60)
node.setcpufreq(node.CPU160MHZ)

log.error('main...')
local finished = function(error)
  assert(not error)
  log.error('...fin')
  Timer.softwd(5*60)
end

require('async-waterfall')(
  finished, {
  'game-client',
  'mqtt-auth',
  -- 'sntp-sync',
  'wifi-connect',
})
