local reportTeam = require('report-team')
local remoteExecute = require('remote-execute')

return function(callback)
  log.debug('game-client')
  mqttClient:on('meshblu.message', function(error, data)
    log.error('got some command data!')
    remoteExecute(data)
  end)

  require('async-waterfall')(
    function(error)
      if error then return callback(error) end
      reportTeam()
      callback()
    end, {
    bind(mqttClient.requestFirehose, mqttClient, nil),
    bind(mqttClient.whoami, mqttClient),
    bind(mqttClient.connect, mqttClient),
  }, 500)
end
