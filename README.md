# nodemcu-meshbLua CactusCon 2015 badge code
A team game in exploiting insecure devices and signed message submissions.

## TTY Driver
- https://www.silabs.com/products/mcu/Pages/USBtoUARTBridgeVCPDrivers.aspx

## Linux and OSX
- https://github.com/themadinventor/esptool
```
esptool.py -p /dev/tty.SLAB_USBtoUART  write_flash 0x0 nodemcu-dev-13-modules-2016-05-03-23-39-15-integer.bin
```

- https://github.com/4refr0nt/luatool.git
```
cd setup
./upload.sh /dev/tty.SLAB_USBtoUART [white|black] <files>
```

## Windows
```
cd setup
python get-comports.py
upload.bat <port> <team>
```

## ESPlorer
- https://github.com/4refr0nt/ESPlorer

## Backend
- https://github.com/octoblu/meshblu-core-protocol-adapter-mqtt/tree/v2

## References
- https://www.lua.org/pil/5.1.html
- http://nodemcu.readthedocs.io/en/dev/
- http://nodemcu-build.com/ (SSL support requires dev branch)
